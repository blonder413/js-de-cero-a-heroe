document.getElementById("correo").addEventListener("focus", agregaEvento);
document.getElementById("correo").addEventListener("keyup", (elemento) => {
    elemento.target.value = elemento.target.value.toUpperCase();
});

function alCargar() {
  if (navigator.cookieEnabled) {
    alert("tiene activadas las cookies");
  } else {
    alert("tiene desactivadas las cookies");
  }
}
function cambiar_texto(titulo) {
  titulo.innerHTML = "Texto cambiado";
}

document.getElementById("titulo").onclick = cambiar_texto2;

function cambiar_texto2(titulo) {
  document.getElementById("mostrar").innerHTML = "cambiado";
}
function alCambiar() {
  let x = document.getElementById("select").value;
  document.getElementById("mostrar").innerHTML = x;
}
function mayusculas(input) {
  input.value = input.value.toUpperCase();
}
function sobre(elemento) {
  elemento.style.color = "red";
  document.getElementById("mostrar").innerHTML = "moviendo sobre";
}
function fuera(elemento) {
  elemento.style.color = "#FF1";
  document.getElementById("mostrar").innerHTML = "fuera";
}
function tieneFoco(elemento) {
  elemento.style.background = "#FAA";
  document.getElementById("mostrar").innerHTML = "tiene foco";
}
function pierdeFoco(elemento) {
  elemento.style.background = "#FFF";
  document.getElementById("mostrar").innerHTML = "pierde foco";
}

function agregaEvento(evento) {
  let componente = evento.target;
  componente.style.background = "#CFC";
}
