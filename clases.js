class Persona {
    static contador = 0
    constructor(nombre, apellido) {
        this._nombre = nombre
        this._apellido = apellido
    }
    get nombre() {
        return this._nombre
    }
    set nombre(nombre) {
        this._nombre = nombre
    }

    nombre_completo()
    {
        return this._nombre + ' ' + this._apellido
    }

    static saludar(nombre = '')
    {
        console.log("hola " + nombre);
    }
}
let claire = new Persona('claire', 'redfield')
claire.nombre = 'chris'
console.log(claire);
console.log(claire.nombre);

class Empleado extends Persona {
    constructor(nombre, apellido, empresa)
    {
        super(nombre, apellido)
        this.empresa = empresa
    }
    nombre_completo()
    {
        return super.nombre_completo() + ' ' + this.empresa
    }
}
barry = new Empleado('barry', 'burton', 'bsaa')
console.log(barry);
console.log(barry.nombre_completo());

Persona.saludar()
Empleado.saludar('blonder')