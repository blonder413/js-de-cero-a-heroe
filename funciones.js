miFuncion(nombre)
{
    return "hola " + nombre
}

let suma = function(a,b) { return a + b }

// se invoca a sí misma
(function (a,b) {
    alert("suma = " + (a+b))
})(4,5)

// función flecha
const sumar = (a,b) => a + b
resultado = sumar(4,13)
console.log(resultado);