window.onload = function() {
    let canvas = document.getElementById('canvas')
    if (canvas && canvas.getContext) {
        ctx = canvas.getContext("2d")
        if (ctx) {
            // gradiente_lineal(ctx)
            // gradiente_radial(ctx)
        } else {
            alert('canvas 2D no soportada')
        }
    }
}

function gradiente_radial(ctx)
{
    x = canvas.width / 2
    y = canvas.height / 2
    r1 = 10
    r2 = 300
    var gradiente = ctx.createRadialGradient(x, y, r1, x, y, r2)
    gradiente.addColorStop(0, 'yellow')
    gradiente.addColorStop(0.75, 'blue')
    gradiente.addColorStop(1, 'red')
    ctx.fillStyle = gradiente
    ctx.fillRect(0, 0, canvas.width, canvas.height)
}

function gradiente_lineal(ctx)
{
    var gradiente = ctx.createLinearGradient(0, 0, canvas.width, 0)
    gradiente.addColorStop(0, 'yellow')
    gradiente.addColorStop(0.75, 'blue')
    gradiente.addColorStop(1, 'red')
    ctx.fillStyle = gradiente
    ctx.fillRect(0, 0, canvas.width, canvas.height)
}