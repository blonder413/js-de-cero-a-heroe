window.onload = function() {
    let archivoSelect = document.getElementById('archivo')
    let canvas = document.getElementById('canvas')
    if (canvas && canvas.getContext) {
        ctx = canvas.getContext("2d")
        var imagen
        if (ctx) {
            archivoSelect.onchange = function(e)
            {
                if (archivoSelect.value != '') {
                    imagen.src = '../' + archivoSelect.value
                    imagen.onload = function(e) {
                        procesaImagen(imagen)
                    }
                }
            }
            imagen = new Image()
            imagen.src = '../foto.jpg'
            imagen.onload = function(e)
            {
                procesaImagen(imagen)
            }
        } else {
            alert('canvas 2D no soportada')
        }
    }
}

function procesaImagen(imagen)
{
    limpiar(imagen)
    ctx.drawImage(imagen, 10, 10)
}

function limpiar(imagen)
{
    ctx.clearRect(0, 0, imagen.width, imagen.height)
}