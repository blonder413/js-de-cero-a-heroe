window.onload = function() {
    let canvas = document.getElementById('canvas')
    if (canvas && canvas.getContext) {
        ctx = canvas.getContext("2d")
        if (ctx) {
            ctx.font = '7em Verdana'
            ctx.lineWidth = 1
            ctx.fillStyle = 'blue'
            ctx.strokeStyle = 'yellow'
            ctx.shadowColor = '#000'
            ctx.shadowOffsetX = 5
            ctx.shadowOffset = -5
            ctx.shadowBlur = 10
            ctx.fillText('Blonder413', 15, 200)
            ctx.strokeText('Blonder413', 15, 200)
            // ctx.fillRect(15, 200, 10, 10)
        } else {
            alert('canvas 2D no soportada')
        }
    }
}