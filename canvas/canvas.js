window.onload = function() {
    let canvas = document.getElementById('canvas')
    if (canvas && canvas.getContext) {
        ctx = canvas.getContext("2d")
        if (ctx) {
            // rectangulos(ctx)
            // lineas(ctx)
            // arco(ctx)
            // zigzag(ctx)
            // espiral(ctx)
            // estrella(ctx)
            // engranaje(ctx)
        } else {
            alert('canvas 2D no soportada')
        }
    }
}

function arco(ctx)
{
    ctx.beginPath()
    ctx.arc(70, 220, 40, 1.1 * Math.PI, 1.9 * Math.PI)
    ctx.stroke()
    ctx.beginPath()
    // x, y, radio, inicio ángulo, fin ángulo, sentido contra reloj
    ctx.arc(70, 220, 40, 1.1 * Math.PI, 1.9 * Math.PI, true)
    ctx.stroke()

    // curva bezier
    ctx.beginPath()
    ctx.moveTo(250, 20)
    // x de la curva, y de la curva, x final, y final
    ctx.quadraticCurveTo(500, 150, 550, 20)
    ctx.stroke()

    // curva bezier cúbica
    ctx.lineWidth = 5
    ctx.strokeStyle = 'blue'
    ctx.beginPath()
    ctx.moveTo(300, 70)
    // x p1, y p1, x p2, y p2, x final, y final
    ctx.bezierCurveTo(400, 150, 450, 70, 550, 120)
    ctx.stroke()
}

function zigzag(ctx)
{
    let posicion_x = 120
    let posicion_y = 150
    let desplazamiento = 30
    ctx.lineWidth = 5
    ctx.strokeStyle = 'green'
    ctx.lineCap = 'round'
    ctx.lineJoin = 'round'
    ctx.beginPath(posicion_x, posicion_y)
    for (i = 1; i <= 15; i++) {
        x = posicion_x + i * desplazamiento
        y = posicion_y
        if (i % 2 != 0) {
            y = posicion_y + 100
        }
        ctx.lineTo(x, y)
    }
    ctx.stroke()
}

function espiral(ctx)
{
    var radio = 0, angulo = 0
    ctx.lineWidth = 5
    ctx.strokeStyle = 'rgba(3, 230, 230, 0.8)'
    ctx.beginPath()
    ctx.moveTo(70, 340)
    for (i = 0; i < 200; i++){
        radio += 0.3
        angulo += (Math.PI*2) / 50
        x = 70 + radio * Math.cos(angulo)
        y = 340 + radio * Math.sin(angulo)
        ctx.lineTo(x,y)
    }
    ctx.stroke()
}

function estrella()
{
    var puntas = 10
    var vertices = puntas * 2
    var angulo_estrella = Math.PI * 2 / vertices
    var radio_interno = 30
    var radio_externo = 60
    ctx.strokeStyle = 'rgba(230, 230, 230, 0.8)'
    ctx.fillStyle = 'rgba(180, 200, 200, 0.8)'
    ctx.beginPath()
    for (i = 0 ; i < vertices; i++) {
        var x,y
        if (i % 2 == 0) {
            x = Math.cos(angulo_estrella * i) * radio_externo
            y = Math.sin(angulo_estrella * i) * radio_externo
        } else {
            x = Math.cos(angulo_estrella * i) * radio_interno
            y = Math.sin(angulo_estrella * i) * radio_interno
        }
        ctx.lineTo(210 + x, 330 + y)
    }
    ctx.closePath()
    ctx.fill()
    ctx.stroke()
}

function engranaje(ctx)
{
    var puntos = 30
    ctx.beginPath()
    ctx.lineJoin = 'bevel'
    for (i = 0; i < puntos; i++) {
        var radio = null
        if (i % 2 == 0) {
            radio = 60
        } else {
            radio = 30
        }
        var angulo = Math.PI * 2 / puntos * (i + 1)
        var x = (radio * Math.sin(angulo)) + 350
        var y = (radio * Math.cos(angulo)) + 330
        if (i == 0) {
            ctx.moveTo(x, y)
        } else {
            ctx.lineTo(x, y)
        }
    }
    ctx.closePath()
    ctx.strokeStyle = 'rgba(150, 50, 100, 0.8)'
    ctx.fillStyle = 'rgba(200, 150, 200, 0.8)'
    ctx.fill()
    ctx.closePath()
    ctx.stroke()
}

function lineas(ctx)
{
    ctx.lineWidth = 25
    ctx.strokeStyle = "rgba(217, 217, 40, .85)"
    ctx.beginPath()
    // ctx.lineCap = 'round'
    // ctx.lineCap = 'square'
    ctx.lineCap = 'butt'
    ctx.moveTo(20, 150)
    ctx.lineTo(120, 150)
    ctx.stroke()

    ctx.beginPath()
    ctx.lineWidth = 5
    ctx.moveTo(150, 80)
    ctx.lineTo(200, 30)
    ctx.lineTo(250, 80)
    ctx.lineTo(200, 130)
    ctx.fill()
    //ctx.closePath() // cierra la figura
    ctx.stroke()
}

function rectangulos(ctx)
{
    ctx.fillStyle = "rgba(33, 215, 33, .75)"
    ctx.fillRect(20,20, 100, 100) // x, y, ancho, alto
    ctx.strokeStyle = '#55f'
    ctx.lineWidth = 5
    ctx.strokeRect(20, 20, 100, 100)
}