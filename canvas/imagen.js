window.onload = function() {
    let canvas = document.getElementById('canvas')
    if (canvas && canvas.getContext) {
        ctx = canvas.getContext("2d")
        if (ctx) {
            imagen(ctx)
            // patrones(ctx)
        } else {
            alert('canvas 2D no soportada')
        }
    }
}

function imagen(ctx)
{
    let imagen = new Image(100,  100)
    imagen.src = '../logo.png'
    imagen.onload = function(e)
    {
        procesar_imagen()
    }

    function procesar_imagen()
    {
        ctx.drawImage(imagen, 10, 10)
    }
}

function patrones(ctx)
{
    let bola = new Image(100,200)
    bola.src = '../logo.png'
    console.log(bola)
    bola.onload = function(e)
    {
        let patron = ctx.createPattern(bola, 'repeat')
        ctx.fillStyle = patron
        ctx.fillRect(0, 0, canvas.width, canvas.height)
    }
}