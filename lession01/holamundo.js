var nombre = 'blonder'
console.log(nombre)

// tipo de dato string
console.log(typeof(nombre))

// number
var numero = 4.13
console.log(typeof(numero));

// objeto
var objeto = {
    "nombre": "blonder",
    "web": "blonder413.wordpress.com"
}
console.log(objeto);
console.log(typeof objeto);

// bolean
vivo = true
console.log(vivo);
console.log(typeof vivo);

// función
function miFuncion(){}
console.log(typeof miFuncion);

// la clase es considerda función
class Persona {
    constructor(nombre, apellido)
    {
        this.nombre = nombre
        this.apellido = apellido
    }
}

console.log(typeof Persona);

var x
console.log(typeof x);

// tipo objeto
var y = null
console.log(typeof y);

// arreglos (lo toma como objeto)
var autos = ['bmw', 'audi', 'volvo']
console.log(autos);
console.log(typeof autos);

var z = NaN
console.log(typeof z);