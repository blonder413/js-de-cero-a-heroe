let a=4, b=13

suma = a+b
resta = a-b
multi = a*b
div = a/b
modulo = a%b
potencia = 2**3

console.log(suma);
console.log(resta);
console.log(multi);
console.log(div);
console.log(modulo);
console.log(potencia);

/*
a++ -> ejecuta la operación y luego incrementa
++a -> incrementa y luego ejecuta la operación
*/

console.log(++a);

// operadores de incremento
a+=1
a-=2
a*=2
a/=2

// operadores de comparación
respuesta = a == b
respuesta = a === b
respuesta = a > b
respuesta = a >= b
respuesta = a < b
respuesta = a <= b
respuesta = a != b