let persona = {
    nombre: 'jill',
    apellido: 'valentine',
    email: 'jvalentine@bsaa.org',
    nombre_completo: function() {
        return this.nombre + ' ' + this.apellido
    },
    // se recomienda usar get
    get nombre_apellido() {
        return this.nombre + ' ' + this.apellido
    }
}
console.log(persona.nombre_completo())
console.log(persona.nombre_apellido);
delete persona.nombre_completo
console.log(persona);

persona2 = new Object()
persona2.nombre = 'claire'
persona2.apellido = 'redfield'

console.log(persona2.nombre, persona2['apellido'])

for (propiedad in persona) {
    console.log(persona[propiedad])
}

console.log(Object.values(persona2))

console.log(JSON.stringify(persona))

// getter y setter
let persona3 = {
    nombre: 'ethan',
    idioma: 'es',
    get lang() {
        return this.idioma.toUpperCase()
    },
    set lang(lang) {
        this.idioma = lang.toUpperCase()
    }
}
persona3.lang = 'en'
console.log(persona3.lang);
// ----------------------------------------------------------------------
// función constructor
function Persona(nombre, apellido, email)
{
    this.nombre = nombre
    this.apellido = apellido
    this.email = email
    this.nombre_completo = function() {
        return this.nombre + ' ' + this.apellido
    }
}
// prototype agrega atributos a todos los objetos
Persona.prototype.vivo = true

let barry = new Persona('barry', 'burton', 'bburton@bsaa.org')
barry.telefono = '52352523'
console.log(barry);
console.log(barry.nombre_completo())
// ---------------------------------------------------------------------
let persona4 = {
    nombre: 'leon',
    apellido: 'kennedy',
    // call no funciona con get
    nombre_completo: function() {
        return this.nombre + ' ' + this.apellido
    },
}

let persona5 = {
    nombre: 'chris',
    apellido: 'redfield'
}
console.log(persona4.nombre_completo.call(persona5))
// ----------------------------------------------------------------------
let persona6 = {
    nombre: 'leon',
    apellido: 'kennedy',
    // call no funciona con get
    datos: function(profesion, telefono) {
        return profesion + ' ' + telefono
    },
}
console.log(persona6.datos('padre', 34234))